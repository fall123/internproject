﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace InternWebServer.Models
{
	public class SignUpModel
	{
		public string userId { get; set; }
		public string userPassword { get; set; }
		public string userName { get; set; }
		public string userBirth { get; set; }
		public string userEmail { get; set; }
		public string userPhone { get; set; }
		public char userGender { get; set; }
		public SignUpModel()
		{
		}
		public SignUpModel(string userId, string userPassword, string userName, string userBirth, string userEmail, string userPhone, string userGender)
		{
			this.userId = userId;
			this.userPassword = userPassword;
			this.userName = userName;
			this.userBirth = userBirth;
			this.userEmail = userEmail;
			this.userPhone = userPhone;
			this.userGender = (userGender == "성별" ? 'n' : Convert.ToChar(userGender));
		}
		public bool CheckId()
		{
			Regex regex = new Regex(@"^[0-9a-zA-Z]{4,20}$");
			if (!regex.IsMatch(userId)) return false;
			return true;
		}

		public bool CheckPw()
		{
			Regex regex = new Regex(@"^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\W]).{8,20}$");
			if (!regex.IsMatch(userPassword)) return false;
			return true;
		}
		public bool CheckEmail()
		{
			Regex regex = new Regex(@"^[^@\s]+@[^@\s]+\.[^@\s]+$");
			if (userEmail != "" && !regex.IsMatch(userEmail)) return false;
			return true;
		}

		public bool CheckPhone()
		{
			Regex regex = new Regex(@"^[0-9]{11}$");
			if (userPhone != "" && !regex.IsMatch(userPhone)) return false;
			return true;
		}
		public bool CheckGender()
		{
			if (userGender == 'n') return false;
			return true;
		}

		public string CheckSingup()
		{
			if (!CheckId()) return "올바른 아이디를 입력해주세요.";
			if (!CheckPw()) return "올바른 비밀번호를 입력해주세요.";
			if (!CheckEmail()) return "올바른 이메일을 입력해주세요.";
			if (!CheckPhone()) return "올바른 전화번호를 입력해주세요.";
			if (!CheckGender()) return "올바른 성별을 선택해 주세요.";
			return "success";
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternWebServer.Models
{
	public enum submitType : int
	{
		TypeSignUp = 1,
		TypeLogin = 2,
		
		TypeTwitchFollowSearch = 11
	}
}

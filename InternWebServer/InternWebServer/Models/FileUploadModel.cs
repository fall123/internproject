﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InternWebServer.Models
{
	public class FileUploadModel  //사용중지
	{
		public string filePath { get; set; }
		public string fileName { get; set; }

		public IFormFile file;

		public FileUploadModel(string filePath, string fileName)
		{
			this.fileName = fileName;
			this.filePath = filePath;
		}

		public FileUploadModel(string fileName)
		{
			this.fileName = fileName;
			filePath = System.IO.Path.Combine(Environment.CurrentDirectory, fileName);
		}

		/*public string SaveInLocal()
		{
			var k = 1;

			while(System.IO.File.Exists(filePath))
			{
				var name = fileName.Substring(0, fileName.LastIndexOf("."));
				var ext = fileName.Substring(fileName.LastIndexOf(".") + 1);
				filePath = System.IO.Path.Combine(Environment.CurrentDirectory, $"{name} ({k++}).{ext}");
			}

			using (var uploadFile = System.IO.File.Create(filePath))
			{
				
			}
		}*/
	}
}

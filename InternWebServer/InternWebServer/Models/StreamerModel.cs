﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternWebServer.Models
{
	public class StreamerModel
	{
		public string streamerId { get; set; }
		public string streamerName { get; set; }
		public string gameName { get; set; }
		public string gameId { get; set; }
		public string language { get; set; }
		public string title { get; set; }

		public StreamerModel(string id, string name)
		{
			streamerId = id;

			streamerName = name;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InternWebServer.Models;
using Microsoft.AspNetCore.Routing;
using Renci.SshNet;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Web;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace InternWebServer.Controllers
{
	public class HomeController : Controller
	{
		private readonly ILogger<HomeController> _logger;

		System.Threading.Thread streamerChannalThread;

		void scheduledTask()
		{
			DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
			List<StreamerModel> streamerModelList = context.GetStreamer();

			TwitchApiBot twitchapi = new TwitchApiBot("kkeszigw9uief19zphyu685snsfy9x", "k3mmn7ez4t4btccbexc2xa3kepmsji");

			while(true)
			{
				foreach (StreamerModel s in streamerModelList)
				{
					twitchapi.GetStreamerChannel(s);
					context.InsertStreamerChannel(s);
				}
				System.Threading.Thread.Sleep(TimeSpan.FromHours(1));
			}
		}

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		[Route("/")]
		[Route("/login")]
		public IActionResult login()
		{
			streamerChannalThread = new System.Threading.Thread(scheduledTask);
			streamerChannalThread.Start();

			ViewData["title"] = "로그인";
			return View("~/Views/Home/login.cshtml");
		}

		[Route("/signup")]
		public IActionResult SignUp()
		{
			ViewData["title"] = "회원 가입";
			return View("~/Views/Home/signup.cshtml");
		}

		[Route("/main")]
		public IActionResult MainPage()
		{
			ViewData["title"] = "메인 페이지";
			return View("~/Views/Home/LoginAfterIndex.cshtml");
		}

		[Route("/upload")]

		public IActionResult FileUploadView()
		{
			ViewData["title"] = "파일 업로드";
			return View("~/Views/Home/fileUpload.cshtml");
		}

		[Route("/submit")]
		public IActionResult SubmitTypeCheck()
		{
			var Type = (submitType)Convert.ToInt32(Request.Query["type"]);

			switch(Type)
			{
				case submitType.TypeSignUp:
					{
						SignUpModel signUp = new SignUpModel(
							Request.Form["userid"], 
							Request.Form["userpw"], 
							Request.Form["username"], 
							Request.Form["userbirth"], 
							Request.Form["useremail"], 
							Request.Form["userphone"], 
							Request.Form["usergender"]
							);

						string _CheckResult = signUp.CheckSingup();

						if (_CheckResult != "success")
						{
							ViewData["result"] = _CheckResult;
							ViewData["title"] = "회원 가입";
							return View("~/Views/Home/signup.cshtml");
						}

						if (Request.Form["userpw"] != Request.Form["userpw2"])
						{
							ViewData["result"] = "비밀번호가 일치하지 않습니다.";
							ViewData["title"] = "회원 가입";
							return View("~/Views/Home/signup.cshtml");
						}

						DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
						if(context.AlreadyHasId(signUp.userId))
						{
							ViewData["result"] = "이미 존재하는 아이디 입니다.";
							ViewData["title"] = "회원 가입";
							return View("~/Views/Home/signup.cshtml");
						}

						if(context.InsertSignup(signUp))
						{
							return Redirect("/login");
						}
						else
						{
							ViewData["result"] = "회원가입에 실패했습니다.";
							ViewData["title"] = "회원 가입";
							return View("~/Views/Home/signup.cshtml");
						}
					}
				case submitType.TypeLogin:
					{
						DBService context = HttpContext.RequestServices.GetService(typeof(DBService)) as DBService;
						bool result = context.LoginCheck(Request.Form["id"], Request.Form["pw"]);
						if(result)
						{
							return Redirect("/main");
						}
						else
						{
							return Json("로그인 실패");
						}
					}
				case submitType.TypeTwitchFollowSearch:
					{
						string streamerName = Request.Form["streamerName"];
						if (streamerName != "")
						{
							ViewData["IsSearch"] = true;
							TwitchApiBot context = HttpContext.RequestServices.GetService(typeof(TwitchApiBot)) as TwitchApiBot;
							JObject Streamer = context.GetStreamerInfo(streamerName);
							JObject StreamUser = context.GetUser(Streamer["data"][0]["id"].ToString());
							ViewData["StreamerImg"] = Streamer["data"][0]["thumbnail_url"];
							ViewData["StreamerId"] = StreamUser["data"][0]["display_name"];
							ViewData["IsBroadCast"] = (bool)Streamer["data"][0]["is_live"];
							ViewData["BTitle"] = Streamer["data"][0]["title"];
							ViewData["followerCount"] = context.GetStreamerFollowerCount(Streamer["data"][0]["id"].ToString());
							ViewData["BSite"] = "https://www.twitch.tv/" + Streamer["data"][0]["display_name"];
						}
						else
						{
							ViewData["IsSearch"] = false;
						}
						return View("~/Views/Home/TwitchFollow.cshtml");
					}
				default:
					{
						return Redirect("/login");
					}
			}
		}

		[Route("/twitchfollow")]
		public IActionResult TwitchFollow()
		{
			ViewData["title"] = "트위치 팔로워 수";
			ViewData["IsSearch"] = false;
			return View("~/Views/Home/TwitchFollow.cshtml");
		}

		[Route("/twitchchannel")]
		public IActionResult TwitchChannel()
		{
			TwitchApiBot context = HttpContext.RequestServices.GetService(typeof(TwitchApiBot)) as TwitchApiBot;
			ViewData["cid"] = context.Client_Id;
			ViewData["at"] = context.Access_Token;
			ViewData["title"] = "트위치 채널 정보";
			return View("~/Views/Home/TwitchChannel.cshtml");
		}

		[Route("/twitchtaskset")]
		public IActionResult TwitchTaskSetting()
		{
			ViewData["title"] = "스케줄러 셋팅";
			return View("~/Views/Home/TaskSetting.cshtml");
		}

		//다수의 파일을 한번에 업로드 하는 기능이지만 일단 한개씩 업로드 하는 방향으로 사용하기로 함.
		public IActionResult UploadFiles(List<IFormFile> files)
		{
			var uploadPath = Environment.CurrentDirectory;
			var response = "";
			try
			{
				files.ForEach(file => {
					var filePath = System.IO.Path.Combine(uploadPath, file.FileName);
					var k = 1;

					while (System.IO.File.Exists(filePath))
					{
						var name = file.FileName.Substring(0, file.FileName.LastIndexOf("."));
						var ext = file.FileName.Substring(file.FileName.LastIndexOf(".") + 1);
						filePath = System.IO.Path.Combine(uploadPath, $"{name} ({k++}).{ext}");
					}
					using (var uploadFile = System.IO.File.Create(filePath))
					{
						file.CopyTo(uploadFile);
					}

					FTPManager context = HttpContext.RequestServices.GetService(typeof(FTPManager)) as FTPManager;
					response = context.UploadFile(filePath);

					FileInfo fileinfo = new FileInfo(filePath);
					fileinfo.Delete();
				});
			}
			catch (Exception ex)
			{
				response = ex.Message;
			}

			return Json(response);
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}

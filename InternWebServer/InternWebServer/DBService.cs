﻿using InternWebServer.Models;
using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Cms;
using Renci.SshNet.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternWebServer
{
	public class DBService
	{
		public string ConnectionString { get; set; }

		public DBService(string connectionString)
		{
			this.ConnectionString = connectionString;
		}

		private MySqlConnection GetConnection()
		{
			return new MySqlConnection(ConnectionString);
		}

		public List<SignUpModel> GetData()
		{
			List<SignUpModel> list = new List<SignUpModel>();
			string SQL = "SELECT * FROM user ";
			using (MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				using(var reader = cmd.ExecuteReader())
				{
					while(reader.Read())
					{
						list.Add(new SignUpModel()
						{
							userId = reader["userid"].ToString(),
							userPassword = reader["userpw"].ToString(),
							userName = reader["username"].ToString(),
							userBirth = reader["userbirth"].ToString(),
							userEmail = reader["useremail"].ToString(),
							userPhone = reader["userphone"].ToString(),
							userGender = Convert.ToChar(reader["usergender"])
						});
					}
				}
				conn.Close();
			}
			return list;
		}

		public bool AlreadyHasId(string userId)
		{
			string SQL = "SELECT userid FROM user WHERE userid = '" + userId + "' limit 1 ";
			bool result = false;

			using (MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				using (var reader = cmd.ExecuteReader())
				{
					if(reader.Read())
					{
						result = true;
					}

				}
				conn.Close();
			}
			return result;
		}

		
		public bool InsertSignup(SignUpModel signUpModel)
		{
			string SQL = "INSERT INTO user(userid, userpw, username, userbirth, useremail, userphone, usergender) VALUE('" +
				signUpModel.userId + "', password('" +
				signUpModel.userPassword + "'), '" +
				signUpModel.userName + "', '" +
				signUpModel.userBirth + "', '" +
				signUpModel.userEmail + "', '" +
				signUpModel.userPhone + "', '" +
				signUpModel.userGender + "' ) ";

			bool result = true;

			using (MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				if(cmd.ExecuteNonQuery() != 1)
				{
					result = false;
				}

				conn.Close();
			}
			return result;
		}

		public bool LoginCheck(string userid, string userpw)
		{
			string SQL = "SELECT * FROM user WHERE userid = '" + userid + "' AND userpw = password('" + userpw + "') ";

			bool result = true;

			using(MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				using(var reader = cmd.ExecuteReader())
				{
					if(!reader.Read())
					{
						result = false;
					}
				}

				conn.Close();
			}
			return result;
		}

		public List<StreamerModel> GetStreamer()
		{
			List<StreamerModel> list = new List<StreamerModel>();

			string SQL = "SELECT * FROM streamer";

			using (MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						list.Add(new StreamerModel(reader["streameridx"].ToString(), reader["streamername"].ToString()));
					}
				}
				conn.Close();
			}
			return list;
		}

		public bool InsertStreamerChannel(StreamerModel streamerModel)
		{
			string SQL = "INSERT INTO streamerchannel(streameridx, gamename, gameid, lang, title) VALUE(" +
				streamerModel.streamerId + ", \"" +
				streamerModel.gameName + "\", \"" +
				streamerModel.gameId + "\", \"" +
				streamerModel.language + "\", \"" +
				streamerModel.title + "\" ) ";

			bool result = true;

			using (MySqlConnection conn = GetConnection())
			{
				conn.Open();
				MySqlCommand cmd = new MySqlCommand(SQL, conn);

				if (cmd.ExecuteNonQuery() != 1)
				{
					result = false;
				}

				conn.Close();
			}
			return result;
		}
	}
}

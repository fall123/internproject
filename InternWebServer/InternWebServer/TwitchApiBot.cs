﻿using InternWebServer.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InternWebServer
{
	public class TwitchApiBot
	{
		public string Client_Id { get; set; }
		public string Client_Secret { get; set; }
		public string Access_Token { get; set; }

		public TwitchApiBot(string Client_id, string Client_secret)
		{
			this.Client_Id = Client_id;
			this.Client_Secret = Client_secret;
			this.Access_Token = GetAccessToken();
		}

		public string GetAccessToken()
		{
			string URL = "https://id.twitch.tv/oauth2/token";

			StringBuilder dataParams = new StringBuilder();
			dataParams.Append("client_id=");
			dataParams.Append(Client_Id);
			dataParams.Append("&client_secret=");
			dataParams.Append(Client_Secret);
			dataParams.Append("&grant_type=client_credentials");

			byte[] byteDataParams = UTF8Encoding.UTF8.GetBytes(dataParams.ToString());

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = byteDataParams.Length;

			Stream stDataParams = request.GetRequestStream();
			stDataParams.Write(byteDataParams, 0, byteDataParams.Length);
			stDataParams.Close();

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			Stream stReadData = response.GetResponseStream();
			StreamReader srReadData = new StreamReader(stReadData, Encoding.Default);

			string strResult = srReadData.ReadToEnd();

			JObject GetToken = JObject.Parse(strResult);

			return (string)GetToken["access_token"];
		}

		public string UseApi(string URI, string sb = "")
		{
			if (sb != "") URI += sb;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URI);
			request.Method = "GET";
			request.Headers["Authorization"] = "Bearer " + Access_Token;
			request.Headers["Client-ID"] = Client_Id;

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			Stream stReadData = response.GetResponseStream();
			StreamReader srReadData = new StreamReader(stReadData, Encoding.Default);

			string strResult = srReadData.ReadToEnd();

			return strResult;
		}

		public JObject GetLiveStreams()
		{
			string Result = UseApi("https://api.twitch.tv/helix/streams");

			return JObject.Parse(Result) ;
		}

		public JObject GetUser(string userid)
		{
			StringBuilder dataParams = new StringBuilder();
			dataParams.Append("?id=");
			dataParams.Append(userid);

			string Result = UseApi("https://api.twitch.tv/helix/users", dataParams.ToString());

			return JObject.Parse(Result);
		}

		public JObject GetStreamerInfo(string text)
		{
			string URI = "https://api.twitch.tv/helix/search/channels";

			StringBuilder dataParams = new StringBuilder();
			dataParams.Append("?query=");
			dataParams.Append(text);

			string Result = UseApi(URI, dataParams.ToString());

			return JObject.Parse(Result);
		}

		public int GetStreamerFollowerCount(string text)
		{
			string URI = "https://api.twitch.tv/helix/users/follows";
			StringBuilder dataParams = new StringBuilder();
			dataParams.Append("?to_id=");
			dataParams.Append(text);

			JObject Result = JObject.Parse(UseApi(URI, dataParams.ToString()));

			return (int)Result["total"];
		}

		public void GetStreamerChannel(StreamerModel streamer)
		{
			string URI = "https://api.twitch.tv/helix/channels";
			StringBuilder dataParams = new StringBuilder();
			dataParams.Append("?broadcaster_id=");
			dataParams.Append(streamer.streamerId);

			JObject Result = JObject.Parse(UseApi(URI, dataParams.ToString()));

			streamer.gameId = (string)Result["data"][0]["game_id"];
			streamer.gameName = (string)Result["data"][0]["game_name"];
			streamer.language = (string)Result["data"][0]["broadcaster_language"];
			streamer.title = (string)Result["data"][0]["title"];
		}
	}
}

﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InternWebServer
{
	public class FTPManager
	{

		public delegate void ExceptionEventHandler(string LocationID, Exception ex);
		public event ExceptionEventHandler ExceptoinEvent;

		public Exception LastExeption = null;

		public bool inConnected { get; set; }

		private readonly string ipAddr = string.Empty;
		private readonly string userId = string.Empty;
		private readonly string pwd = string.Empty;

		public FTPManager(string ip, string userId, string pwd)
		{
			this.ipAddr = ip;
			this.userId = userId;
			this.pwd = pwd;
		}

		public string ConnectToServer()
		{
			this.inConnected = false;
			long filecount = 0;

			try
			{
				using(var sftp = new SftpClient(ipAddr,userId,pwd))
				{
					sftp.Connect();
					var file = sftp.ListDirectory(".");
					filecount = file.Count();
				}

				this.inConnected = true;
			}
			catch(Exception ex)
			{
				this.LastExeption = ex;

				System.Reflection.MemberInfo Info = System.Reflection.MethodInfo.GetCurrentMethod();
				string id = string.Format("{0}.{1}", Info.ReflectedType.Name, Info.Name);

				if (this.ExceptoinEvent != null)
				{
					ExceptoinEvent(id, ex);
				}

				return "false";
			}

			return filecount.ToString();
		}

		public string UploadFile(string localFileName)
		{
			try
			{
				using var sftp = new SftpClient(ipAddr, userId, pwd);
				sftp.Connect();

				using (var file = File.OpenRead(localFileName))
				{
					sftp.UploadFile(file, System.IO.Path.GetFileName(localFileName));
				}

				sftp.Disconnect();
			}
			catch (Exception ex)
			{
				this.LastExeption = ex;

				System.Reflection.MemberInfo Info = System.Reflection.MethodInfo.GetCurrentMethod();
				string id = string.Format("{0}.{1}", Info.ReflectedType.Name, Info.Name);

				if (this.ExceptoinEvent != null)
				{
					ExceptoinEvent(id, ex);
				}

				return "false";
			}

			return "파일 업로드 성공\n경로 : localhost/" + System.IO.Path.GetFileName(localFileName);
		}
	}
}
